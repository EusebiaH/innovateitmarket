// Ruleaza client
cd client
ng serve --ssl true --ssl-cert "./ssl/localhost.pem" --ssl-key "./ssl/localhost-key.pem"

// Ruleaza backend
cd API
dotnet watch --no-hot-reload

// Este necesara conectarea la internet pentru finalizarea platii

// Porneste containere DOCKER
docker-compose up --detach
